fixtures_path = Rails.root.join('app', 'assets', 'images')

places = []
places << Place.create(title: 'Sierra', description: Faker::Lorem.sentence, image: File.new(fixtures_path.join('sierra.jpg')))
places << Place.create(title: 'Adriano coffee', description: Faker::Lorem.sentence, image: File.new(fixtures_path.join('adriano.jpg')))

5.times do
  places << Place.create(
      {
          title: Faker::Name.title,
          description: Faker::Lorem.sentence,
          image: File.new(fixtures_path.join('cafe.jpg'))
      }
  )
end

dishes = []


8.times do
  dishes << Dish.create(title: Faker::Commerce.product_name, price: Faker::Commerce.price, description: Faker::Lorem.sentence, place: places.sample)
end

admin = User.create(name: 'Admin', phone: '0777134134', address: '5th Avenue', email: 'admin@example.com', password: 'adminadmin', password_confirmation: 'adminadmin', admin: true)
user = User.create(name: 'John Doe', phone: '0700567567', address: '6th Avenue', email: 'johndoe@example.com', password: 'asdf1111', password_confirmation: 'asdf1111', admin: false)

