class DisplayController < ApplicationController

    def index
      @places = Place.all
    end

    def show_place
      @place = Place.find(params[:id])
      @dishes_by_place = Dish.where(place_id: params[:id])
    end

end
