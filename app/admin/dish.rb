ActiveAdmin.register Dish do

  permit_params :place_id, :title, :price, :description
  
    form do |f|
      f.inputs do
        f.input :title
        f.input :description
        f.input :price
        f.input :place
      end
      f.actions
    end


    index do
      selectable_column
      id_column
      column :price
      column :title do |dish|
        link_to dish.title, admin_dish_path(dish)
      end
      actions
    end

    show do
      attributes_table do
        row :price
        row :title
        row :place
        row :description
      end
      active_admin_comments
    end

end
