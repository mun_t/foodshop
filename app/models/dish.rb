class Dish < ActiveRecord::Base
  belongs_to :place

  validates :title, presence: true, length: { maximum: 70}
  validates :description, presence: true, length: { maximum: 200}
  validates :price, presence: true, numericality: true
end
